//
//  WealthController.swift
//  Wealth
//
//  Created by Malaar on 2/16/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import UIKit
import Charts


//MARK: - Constants

fileprivate struct Constants {
    static let resourceName = "ngpo"
    static let currentDate = SimpleDate(year: 2014, month: 12, day: 14)
    static let startDate = SimpleDate(year: 2014, month: 1, day: 1)
    static let endDate = SimpleDate(year: 2014, month: 4, day: 1)
}


//MARK: - Controller

class WealthController: UITableViewController {
    
    private var wealthViewModel: WealthViewModel!
    
    @IBOutlet private weak var totalNetLabel: UILabel!
    @IBOutlet private weak var netIncomeLabel: UILabel!
    @IBOutlet private weak var chartView: WealthChartView!
    
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // view-model
        let clientDataProvider = ClientDataProviderJSON(json: Constants.resourceName)
        wealthViewModel = WealthViewModel(clientDataProvider: clientDataProvider)

        // Entries for chart
        let entries = wealthViewModel.chartEntries(startDate: Constants.startDate, endDate: Constants.endDate)
        chartView.configureChart(entriesNumber: entries.count)
        chartView.setupChartData(entries: entries)

        // total net worth
        let totalNetStr = wealthViewModel.totalNetString(at: Constants.currentDate)
        totalNetLabel.attributedText = formatTotalNetString(string: totalNetStr)
        
        // net income ytd
        let netIncomeStr = wealthViewModel.netIncomeString(at: Constants.currentDate)
        netIncomeLabel.attributedText = formatNetIncomeString(string: netIncomeStr)
    }
    
    
    //MARK: - Formatting attributed string
    
    private func formatTotalNetString(string: String) -> NSAttributedString {
        let attributes = [
            NSAttributedStringKey.foregroundColor: UIColor(hex: "4a4a4a"),
            NSAttributedStringKey.font: UIFont(name: "HelveticaNeue", size: 28)!
        ]
        let attributedStr = NSMutableAttributedString(string: string, attributes: attributes)
        if string.hasSuffix(" k") || string.hasSuffix(" m") || string.hasSuffix(" b") || string.hasSuffix(" t") {
            let attributes = [
                NSAttributedStringKey.foregroundColor: UIColor(hex: "9b9b9b"),
                NSAttributedStringKey.font: UIFont(name: "HelveticaNeue", size: 22)!
            ]
            attributedStr.addAttributes(attributes, range: NSMakeRange(string.count - 2, 2))
        }
        return attributedStr
    }
    
    private func formatNetIncomeString(string: String) -> NSAttributedString {
        let attributes = [
            NSAttributedStringKey.foregroundColor: UIColor(hex: "228440"),
            NSAttributedStringKey.font: UIFont(name: "HelveticaNeue", size: 28)!
        ]
        let attributedStr = NSMutableAttributedString(string: string, attributes: attributes)
        if string.hasSuffix(" k") || string.hasSuffix(" m") || string.hasSuffix(" b") || string.hasSuffix(" t") {
            let attributes = [
                NSAttributedStringKey.foregroundColor: UIColor(hex: "9b9b9b"),
                NSAttributedStringKey.font: UIFont(name: "HelveticaNeue", size: 22)!
            ]
            attributedStr.addAttributes(attributes, range: NSMakeRange(string.count - 2, 2))
        }
        return attributedStr
    }
}


// MARK: - To measure performance

func measure(call: () -> Void) {
    let start = CFAbsoluteTimeGetCurrent()
    call()
    let end = CFAbsoluteTimeGetCurrent()
    print("\(end - start)")
}
