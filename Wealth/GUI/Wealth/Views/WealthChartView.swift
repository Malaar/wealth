//
//  WealthChart.swift
//  Wealth
//
//  Created by Malaar on 3/22/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import Foundation
import Charts


// MARK: - WealthChartView

class WealthChartView : LineChartView {

    public func configureChart(entriesNumber: Int) {
        self.xAxis.drawGridLinesEnabled = false
        self.xAxis.drawAxisLineEnabled = false
        self.xAxis.labelPosition = .bottom
        self.xAxis.labelTextColor = UIColor.lightGray
        self.xAxis.labelFont = UIFont(name: "Helvetica", size: 14)!
        // need to show data by Quarter
        self.xAxis.setLabelCount(7, force: true)
        self.xAxis.valueFormatter = WealthXAxisFormatterQuarter(numberOfValues: entriesNumber)
        
        let leftAxisY = self.getAxis(YAxis.AxisDependency.left)
        leftAxisY.enabled = false
        let rightAxisY = self.getAxis(YAxis.AxisDependency.right)
        rightAxisY.enabled = true
        rightAxisY.labelTextColor = UIColor.lightGray
        rightAxisY.drawGridLinesEnabled = true
        rightAxisY.drawAxisLineEnabled = false
        rightAxisY.gridLineDashPhase = 0.0
        rightAxisY.gridLineDashLengths = [4, 2]
        rightAxisY.valueFormatter = WealthYAxisFormatter()
        rightAxisY.labelFont = UIFont(name: "Helvetica", size: 14)!
        
        self.chartDescription = nil
        self.legend.enabled = false
        self.isUserInteractionEnabled = false
    }
    
    public func setupChartData(entries: [ChartDataEntry], animate: Bool = true) {
        let dataSet1 = LineChartDataSet(values: entries, label: nil)
        dataSet1.colors = [UIColor(red: 147.0/255.0, green: 32.0/255.0, blue: 117.0/255.0, alpha: 1.0)]
        dataSet1.lineWidth = 1.6
        dataSet1.drawCirclesEnabled = false
        dataSet1.drawValuesEnabled = false
        dataSet1.drawFilledEnabled = true
        dataSet1.highlightEnabled = false
        
        let color1 = UIColor.red
        let color2 = UIColor(white: 1.0, alpha: 0.0)
        let colors = [color1.cgColor, color2.cgColor]
        let colorLocations: [CGFloat] = [0.3, 1.0]
        let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(),
                                  colors: colors as CFArray,
                                  locations: colorLocations)!
        dataSet1.fill = Fill(linearGradient: gradient, angle: -90.0)
        
        let chartData = LineChartData()
        chartData.addDataSet(dataSet1)
        self.data = chartData
        if animate {
            self.animate(yAxisDuration: 0.5)
        }
    }
}
