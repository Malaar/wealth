//
//  ShortifiedNumberFormatter.swift
//  Wealth
//
//  Created by Malaar on 2/17/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import Foundation


// MARK: - ShortifiedNumberFormatter

class ShortifiedNumberFormatter : NumberFormatter {
    
    public override func string(from number: NSNumber) -> String? {
        var value = number.doubleValue
        let sign = value >= 0 ? 1.0 : -1.0
        value = fabs(value)
        
        var suffix = ""
        if value >= 1000_000_000_000 {
            value = value / 1000_000_000_000
            suffix = " t"
        } else if value >= 1000_000_000 {
            value = value / 1000_000_000
            suffix = " b"
        } else if value >= 1000_000 {
            value = value / 1000_000
            suffix = " m"
        } else if value >= 1000 {
            value = value / 1000
            suffix = " k"
        }
        value *= sign
        
        var result = super.string(from: NSNumber(value: value))
        if let string = result, !suffix.isEmpty {
            result = string + suffix
        }
        return result
    }
}
