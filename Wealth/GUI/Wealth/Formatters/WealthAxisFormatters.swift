//
//  WealthEntryFormatter.swift
//  Chart01
//
//  Created by Malaar on 2/15/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import Foundation
import Charts


// MARK: - WealthYAxisFormatter

class WealthYAxisFormatter: IAxisValueFormatter {
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let result: String
        if value >= 1000_000_000 {
            result = "\(Int(value) / 1000_000_000)b"
        } else if value >= 1000_000 {
            result = "\(Int(value) / 1000_000)m"
        } else if value >= 1000 {
            result = "\(Int(value) / 1000)k"
        }
        else {
            result = String(value)
        }
        return result
    }
}


// MARK: - WealthXAxisFormatterQuarter

class WealthXAxisFormatterQuarter: IAxisValueFormatter {
    
    private let numberOfValues: Int
    
    init(numberOfValues: Int) {
        self.numberOfValues = numberOfValues
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let result: String

        func isMiddle(value: Double, start: Int, end: Int) -> Bool {
            if Int(value) >= start && Int(value) < end {
                let midde = start + (end - start) / 2
                return midde > Int(value) - 3 && midde < Int(value) + 3
            }
            return false
        }

        if isMiddle(value: value, start: 0, end: numberOfValues / 3) {
            result = "Jan 2014"
        } else if isMiddle(value: value, start: numberOfValues / 3, end: 2 * numberOfValues / 3) {
            result = "Feb 2014"
        } else if isMiddle(value: value, start: 2 * numberOfValues / 3, end: numberOfValues) {
            result = "Mar 2014"
        }
        else {
            result = ""
        }

        return result
    }
}
