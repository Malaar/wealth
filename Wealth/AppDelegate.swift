//
//  AppDelegate.swift
//  Wealth
//
//  Created by Malaar on 2/16/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
}
