//
//  ClientDataProvider.swift
//  Chart01
//
//  Created by Malaar on 2/15/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import Foundation


// MARK: - ClientDataProviderError

enum ClientDataProviderError: Error {
    case fileNotFound
}


// MARK: - ClientDataProvider

final class ClientDataProviderJSON: ClientDataProvider {
    
    private let resourceFileName: String

    init(json fileName: String) {
        self.resourceFileName = fileName
    }
    
    public func loadData() throws -> Client {
        guard let filePath = Bundle.main.path(forResource: resourceFileName, ofType: "json") else {
            throw ClientDataProviderError.fileNotFound
        }

        let fileURL = URL(fileURLWithPath: filePath)
        let data = try Data(contentsOf: fileURL, options: .mappedIfSafe)
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(ClientDataProviderJSON.dateFormatter)
        let clients = try decoder.decode([Client].self, from: data)
        return clients[0]
    }

    public static let dateFormatter: DateFormatter = {
        let dateFormatterISO8601 = DateFormatter();
        dateFormatterISO8601.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'.000Z'";
        dateFormatterISO8601.locale = Locale(identifier: "en_US")
        dateFormatterISO8601.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatterISO8601
    }()
}
