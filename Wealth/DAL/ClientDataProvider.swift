//
//  ClientDataProvider.swift
//  Wealth
//
//  Created by Malaar on 3/25/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import Foundation

protocol ClientDataProvider {
    func loadData() throws -> Client
}
