//
//  Asset.swift
//  Chart01
//
//  Created by Malaar on 2/15/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import Foundation


// MARK: - Asset

class Asset: Codable {
    
    public let type: String
    public let category: String
    public let currentValuation: AssetValuation
    public let historicalValuations: [SimpleDate: AssetValuation]

    enum CodingKeys: String, CodingKey {
        case type = "assetType"
        case category
        case currentValuation
        case historicalValuations
    }
    
    enum Category: Int {
        case withinStructure
        case outsideStructure
        case incomeAssets
        
        init(string: String) {
            switch string {
            case "ASSETS_WITHIN_STRUCTURE":
                self = .withinStructure
            case "ASSETS_OUTSIDE_STRUCTURE":
                self = .outsideStructure
            case "FIXED_INCOME_ASSETS":
//                self = .incomeAssets
                fallthrough
            default:
                self = .incomeAssets
            }
        }
    }
    
    init(type: String, category: String, currentValuation: AssetValuation, historicalValuations: [SimpleDate: AssetValuation]) {
        self.type = type
        self.category = category
        self.currentValuation = currentValuation
        self.historicalValuations = historicalValuations
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let type = try container.decode(String.self, forKey: .type)
        let category = try container.decode(String.self, forKey: .category)
        let currentValuation = try container.decode(AssetValuation.self, forKey: .currentValuation)
        
        var valuationsContainer = try container.nestedUnkeyedContainer(forKey: .historicalValuations)
        var historicalValuations = [SimpleDate: AssetValuation]()
        while !valuationsContainer.isAtEnd {
            let assetValuation = try valuationsContainer.decode(AssetValuation.self)
            historicalValuations[assetValuation.date] = assetValuation
        }
        
        self.type = type
        self.category = category
        self.currentValuation = currentValuation
        self.historicalValuations = historicalValuations
    }

    public func wealth(on date: SimpleDate) -> Double {
        return historicalValuations[date]?.valuation ?? 0.0
    }

    
    // MARK: - AssetValuation
    
    class AssetValuation: Codable {
        
        public let valuation: Double
        public let date: SimpleDate

        enum CodingKeys: String, CodingKey {
            case date = "valuationDate"
            case valuation = "valuationInCurrency"
        }
        
        init(date: SimpleDate, valuation: Double) {
            self.valuation = valuation
            self.date = date
        }
        
        required init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            let valuation = try container.decode(Double.self, forKey: .valuation)
            self.valuation = valuation

            let dateString = try container.decode(String.self, forKey: .date)
            var range = dateString.startIndex ..< dateString.index(dateString.startIndex, offsetBy: 4)
            let year = Int(dateString[range])!
            range = dateString.index(dateString.startIndex, offsetBy: 5) ..< dateString.index(dateString.startIndex, offsetBy: 7)
            let month = Int(dateString[range])!
            range = dateString.index(dateString.startIndex, offsetBy: 8) ..< dateString.index(dateString.startIndex, offsetBy: 10)
            let day = Int(dateString[range])!
            self.date = SimpleDate(year: year, month: month, day: day)
        }
    }    
}
