//
//  Client.swift
//  Wealth
//
//  Created by Malaar on 3/22/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import Foundation


// MARK: - Client

class Client: Codable {
    
    public var name: String
    public var assets: [Asset]
    private var assetsCategorized: [Asset.Category: [Asset]]
    
    enum CodingKeys: String, CodingKey {
        case name = "clientName"
        case assets
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let name = try container.decode(String.self, forKey: .name)
        let assets = try container.decode([Asset].self, forKey: CodingKeys.assets)
        
        var assetsCategorized = [Asset.Category: [Asset]]()
        var category: Asset.Category
        for asset in assets {
            category = Asset.Category(string:asset.category)
            if assetsCategorized[category] == nil {
                assetsCategorized[category] = [asset]
            } else {
                assetsCategorized[category]!.append(asset)
            }
        }
        
        self.name = name
        self.assets = assets
        self.assetsCategorized = assetsCategorized
    }
    
    public func wealth(on date: SimpleDate, category: Asset.Category? = nil) -> Double {
        var result: Double = 0
        var assetsToCheck = assets
        if let category = category {
            assetsToCheck = assetsCategorized[category] ?? assets
        }
        for asset in assetsToCheck {
            result += asset.wealth(on: date)
        }
        return result
    }
    
    public func wealth(on date: Date, category: Asset.Category? = nil) -> Double {
        return self.wealth(on: SimpleDate(date: date), category: category)
    }
}
