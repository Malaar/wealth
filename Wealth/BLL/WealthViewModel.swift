//
//  WealthViewModel.swift
//  Wealth
//
//  Created by Malaar on 3/22/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import Foundation
import Charts


// MARK: - WealthViewModel

class WealthViewModel {
    
    private let client: Client
    
    init(clientDataProvider: ClientDataProvider) {
        client = try! clientDataProvider.loadData()
    }
    
    public func chartEntries(startDate: SimpleDate, endDate: SimpleDate) -> [ChartDataEntry] {
        var entries = [ChartDataEntry]()
        var nextDate = startDate.toDate()!
        let end = endDate.toDate()!
        var wealth: Double = 0
        var i: Int = 0
        repeat {
            wealth = client.wealth(on: nextDate)
            let entry = ChartDataEntry(x: Double(i), y: wealth)
            entries.append(entry)
            nextDate = Calendar.current.date(byAdding: .day, value: 1, to: nextDate)!
            i += 1
        } while(nextDate < end)
        return entries
    }
    
    public func totalNetString(at date: SimpleDate) -> String {
        let totalNet = client.wealth(on: date)
        WealthViewModel.formatter.numberStyle = .currency
        return WealthViewModel.formatter.string(from: NSNumber(value:totalNet))!
    }
    
    public func netIncomeString(at date: SimpleDate) -> String {
        let startOfYearDate = SimpleDate(year: date.year, month: 1, day: 1)
        let netIncome = client.wealth(on: date) - client.wealth(on: startOfYearDate)
        WealthViewModel.formatter.numberStyle = .decimal
        var netIncomeStr = WealthViewModel.formatter.string(from: NSNumber(value:fabs(netIncome)))!
        if netIncome > 0 {
            netIncomeStr = "↑" + netIncomeStr
        }
        else if netIncome < 0 {
            netIncomeStr = "↓" + netIncomeStr
        }
        return netIncomeStr
    }
    
    private static let formatter: ShortifiedNumberFormatter = {
        let formatter = ShortifiedNumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "gb_GB")
        formatter.minimumFractionDigits = 1
        formatter.maximumFractionDigits = 1
        return formatter
    } ()
}
