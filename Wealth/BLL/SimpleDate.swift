//
//  SimpleDate.swift
//  Wealth
//
//  Created by Malaar on 2/17/18.
//  Copyright © 2018 malaar. All rights reserved.
//

import Foundation


// MARK: - SimpleDate
/**
 * Description Simplified implementation of class to store only date without time
 **/
struct SimpleDate: Hashable, Codable {
    
    private var date: Int

    init(year: Int, month: Int, day: Int) {
        date = (year << 16) | (month << 8) | day
    }
    
    public var year: Int {
        return date >> 16
    }
    
    public var month: Int {
        return (date & (0b11111111 << 8)) >> 8
    }
    
    public var day: Int {
        return date & 0b11111111
    }
    
    public var hashValue: Int {
        return date
    }
    
    public static func == (lhs: SimpleDate, rhs: SimpleDate) -> Bool {
        return lhs.date == rhs.date
    }
    
    public static func != (lhs: SimpleDate, rhs: SimpleDate) -> Bool {
        return lhs.date != rhs.date
    }

    public func description() -> String {
        return "\(self.year)-\(self.month)-\(self.day)"
    }
}


// MARK: - Date from SimpleDate initializers

extension Date {
    
    init(SimpleDate: SimpleDate) {
        self.init()
        var components = DateComponents()
        components.year = SimpleDate.year
        components.month = SimpleDate.month
        components.day = SimpleDate.day
        if let date = Calendar.current.date(from: components) {
            self = date
        }
    }
}


// MARK: - Transformations between SimpleDate and Date

extension SimpleDate {
    
    init(date: Date = Date()) {
        let year = Calendar.current.component(.year, from: date)
        let month = Calendar.current.component(.month, from: date)
        let day = Calendar.current.component(.day, from: date)
        self.init(year: year, month: month, day: day)
    }
    
    func toDate() -> Date? {
        var components = DateComponents()
        components.timeZone = TimeZone(secondsFromGMT: 0)
        components.year = self.year
        components.month = self.month
        components.day = self.day
        return Calendar.current.date(from: components)
    }
}
