# Wealth

Wealth is a simple test application, which load a big json file and show some statistics inside chart.
Main purpose of this project - to show optimization of logic which works with data.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for overview and testing purposes.

### Prerequisites

* Cocoapods [requirements](https://cocoapods.org/)
* Mac OS X 10.12+, Xcode 9+

### Installing

1. Clone Wealth repository

			git clone git@bitbucket.org:Malaar/wealth.git

2. Install required pods

			pod install

3. Done.

## Running

To run on iOS/mac

Navigate to /proj.ios_mac/ and open Wealth.xcworkspace in Xcode

Enjoy.

## Logic details

For domain implementation next classes are used

* Client
* Asset
* AssetValuation

### Data Loading
* JSON parsing was implemented via Codable protocol
* According to measurement Codable works faster than implementation via JSONSerialization (see below).

### Wealth Calculation
To minimize time on search assets and valuations by categories and dates respectively, next entities was used:

1. Hash table (swift dictionary) - to store assets and asset valuation
2. Category - enumeration based on integer raw value (make converting from category string into enumeration during decoding), which gives ability to fast compare 2 categories as 2 integers and faster find out asset by category.
		* `[Categry: [Asset]]` - Dictionary to store categorized assets inside Client 
		* `[SimpleDate: [AssetValuation]]` - Dictionary to store asset valuation inside Asset
3. `SimpleDate` - struct to work with date without time. Store date (represented by year, month and day) in one integer. It gives approach to compare 2 dates fast, and as a result search in dictionary by key of type SimpleDate really fast

### Measurements
All measurements was calculated as average value of 1000 repeats on full set of data provided in file 'ngpo.json' with 30798 lines of json text (887Kb).

#### Data loading without date decoding (just save date as string):
* Codable implementation: _0.199s_
* JSONSerialization: _0.214s_

#### Data loading with date decoding:
Data decoding dramatically change speed of data loading. To make it faster SimpleDate with custom string formatting was used (converting directly from string into SimpleDate).

* Use `NSDateFormatter` to decode from string into `Date`: _6.14E-01 (614ms)_
* Use decoding from string into `SimpleDate`: _2.98E-01 (289ms)_


#### Calculation of wealth based on provided date and without selection any category:
* Use arrays to store assets and valuations, use `Date` to store date: _7.21E-03s_
* Use dictionaries to store assets and valuations, use `SimpleDate` to store date: _1.25E-05s_


**Note:** To get an array of client's wealth for around 90 dates (1 quarter) algorithm needs _3.2ms_ time

### Macros SIMPLEDATE
This macros this in the code only for test performance reasons
Final version uses only `SimpleDate`, not Date class. So, all code inside preprocessor macros block `#elseif` will not compile and you can ignore it.


## Author

* **[Andrew Korshilovskiy](http://www.linkedin.com/in/korshilovskiy)** - development

## License

(c) All rights reserved.
This project is licensed under the **Proprietary Software License** - see the [LICENSE](http://www.binpress.com/license/view/l/358023be402acff778a934083b76b86f) url for details
